package com.example.demo;

import com.example.demo.model.Comment;
import com.example.demo.model.Post;
import com.example.demo.model.Track;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.LikesRepository;
import com.example.demo.repository.NewsRepository;
import com.example.demo.repository.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.mappers.ModelMapper;


@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    RecordRepository recordRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    LikesRepository likesRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override
    public void run(String... args) throws Exception {
        Track t = new Track();
        t.setRouteId("db9e6c45-ebe4-4e95-827a-f00ba3e0e086");
        t.setUserId("q4iOBg1t93R82Moh9oz0kbCfMDZ2");
        recordRepository.save(t);
        Post p = new Post();
        p.setUserId("q4iOBg1t93R82Moh9oz0kbCfMDZ2");
        p.setLocation("gomel");
        p.setText("bla bla bla");
        recordRepository.save(p);

        Comment comment = new Comment();
        comment.setText("Comment Comment =)");
        comment.setUserId("q4iOBg1t93R82Moh9oz0kbCfMDZ2");
        comment.setNews(p);
        commentRepository.save(comment);
    }
}
