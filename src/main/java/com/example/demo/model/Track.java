package com.example.demo.model;

import com.example.demo.service.client.track.model.TrackDetails;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.Map;

@Entity
@Getter
@Setter
@ToString
public class Track extends News {

    private String routeId;

    @Transient
    @JsonUnwrapped
    private TrackDetails trackDetails;
//    @Transient
//    private String routeName;
//    @Transient
//    private String routeImageUrl;
//    @Transient
//    private Double distance;
//    @Transient
//    private Double averageSpeed;
//    @Transient
//    private String bikeType;
//    @Transient
//    private Long duration;

    @SuppressWarnings("unchecked")


    public void setTrackDetails(TrackDetails trackDetails) {
//        this.routeName = trackDetails.getName();
//        this.routeImageUrl = trackDetails.getImageUrl();
//        this.distance = trackDetails.getDistance();
//        this.averageSpeed = trackDetails.getAverageSpeed();
//        this.bikeType = trackDetails.getBikeType();
//        this.duration = trackDetails.getDuration();
        this.trackDetails = trackDetails;
    }
}
