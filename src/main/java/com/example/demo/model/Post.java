package com.example.demo.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@ApiModel(value = "Get post"/*description = "", parent = News.class*/)
public class Post extends News {
    private static final int MAX_VARCHAR_LENGTH = 10485760;

    @ApiModelProperty(notes = "location of the post")
    private String location;

    @ApiModelProperty(notes = "content of the post")
    @Column(columnDefinition = "text")
    private String text;

    @ApiModelProperty(notes = "List of image urls attached to the post")
    @ElementCollection
    private List<String> postImageUrls;
}
