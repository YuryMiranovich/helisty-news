package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@ToString
public class Comment extends Record {

    private String text;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    Record news;
}
