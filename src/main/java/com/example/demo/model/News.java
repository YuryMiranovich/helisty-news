package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.*;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(/*parent = Record.class, subTypes = {Post.class, Track.class}*/)
public class News extends Record {

    @Formula("(Select COUNT(*) from Comment WHERE Comment.NEWS_ID = id)")
    private Integer commentsCount;

    //@JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "news")
   // @LazyCollection(LazyCollectionOption.EXTRA)
    List<Comment> comments;
}
