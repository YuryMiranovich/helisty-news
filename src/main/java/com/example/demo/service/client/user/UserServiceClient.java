package com.example.demo.service.client.user;

import com.example.demo.service.client.user.model.UserDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceClient {

    private final RestTemplate restTemplate;
    private static final String USER_API_PATH = "http://88.212.202.91:9393/api/";

    //TODO; create some sort of cache to avoid repeating rest call to the same user profile
    public Optional<UserDetails> getUserDetailsById(String accessToken, String userId) {
        log.info("Trying to fetch user info, user id: " + userId);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("ACCESS", accessToken);
        HttpEntity<?> request = new HttpEntity<>(null, httpHeaders);

        //have to process exceptions due to RestTemplate implementation
        try {
            ResponseEntity<UserDetails> userDetailsResponse =
                    restTemplate.exchange(USER_API_PATH + "profiles/{id}",
                            HttpMethod.GET, request, UserDetails.class, userId);
            UserDetails userDetails = userDetailsResponse.getBody();
            log.info("User details: " + userDetails);
            return Optional.ofNullable(userDetails);
        } catch (RestClientResponseException ex) {
            log.error("Unable to get user with id: {}, http statusCode: {}", userId, ex.getRawStatusCode());
        }

        //in case of RestTemplate exceptions
        return Optional.empty();
    }
}

