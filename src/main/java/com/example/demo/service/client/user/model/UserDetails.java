package com.example.demo.service.client.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class UserDetails {
    private String firstName;
    private String lastName;
    private String imageUrl;

    @SuppressWarnings("unchecked")
    @JsonProperty(value = "profile", access = JsonProperty.Access.READ_ONLY)
    private void unpackProfile(Map<String, Object> profile) {
        this.firstName = (String) profile.get("firstName");
        this.lastName = (String) profile.get("lastName");
        Map<String, String> image = (Map<String, String>) profile.get("image");
        this.imageUrl = image == null ? null : image.get("imgURL");
    }
}