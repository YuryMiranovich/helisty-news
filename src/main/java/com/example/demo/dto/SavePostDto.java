package com.example.demo.dto;

import com.example.demo.model.Post;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "Save post", description = "DTO for saving post")
public class SavePostDto {

    @ApiModelProperty(notes = "userId of the post author")
    private String userId;

    @ApiModelProperty(notes = "location of the post")
    private String location;

    @ApiModelProperty(notes = "content of the post")
    private String text;

    @ApiModelProperty(notes = "List of image urls attached to the post")
    private List<String> imageUrls;

    public Post convertToPost() {
        Post post = new Post();
        post.setUserId(userId);
        post.setText(text);
        post.setLocation(location);
        post.setPostImageUrls(imageUrls);
        return post;
    }
}
