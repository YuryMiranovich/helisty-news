package com.example.demo.repository;

import com.example.demo.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
//    @Query("select new com.example.demo.dto.SavePostDto(" +
//            "p.id, p.userId,  p.createdDate, count(c), count(l), p.location, p.text)" +
//            " from Post p left join p.comments c left join p.likes l group by p")
//    List<SavePostDto> findAllWithCommentAndLikeCount();
}