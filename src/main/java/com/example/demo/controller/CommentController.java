package com.example.demo.controller;

import com.example.demo.exception.ElementNotFoundException;
import com.example.demo.model.*;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.NewsRepository;
import com.example.demo.repository.RecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/news")
public class CommentController {
    private final CommentRepository commentRepository;
    private final RecordRepository recordRepository;

    @PostMapping(path = "{newsId}/comments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Comment createComment(@PathVariable Long newsId, @RequestBody Comment comment) {
        Optional<Record> news = recordRepository.findByTypeInAndId(Arrays.asList(Post.class.getSimpleName(),
                Track.class.getSimpleName()),newsId);
        return news.map(n -> {
            comment.setNews(n);
            return commentRepository.save(comment);
        }).orElseThrow(() -> new ElementNotFoundException("News with id " + newsId + " not found"));
    }

    @GetMapping(path = "{newsId}/comments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Comment> createComment(@PathVariable Long newsId) {
        return commentRepository.findAllByNewsId(newsId);
    }
}
